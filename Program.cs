﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace dotnetcore
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("dotnet.homework.28 parallel operations");
            var arrays = new[]
            {
                Enumerable.Range(1, 100000).ToArray(),
                Enumerable.Range(1, 1000000).ToArray(),
                Enumerable.Range(1, 10000000).ToArray(),
            };

            var functions = new[]
            {
                SimpleOperation,
                ParallelOperation,
                LinqOperation,
                ThreadOperation
            };

            foreach (var array in arrays)
            {
                foreach (var function in functions) Console.WriteLine(function(array));
            }
        }

        static Result ThreadOperation(int[] array)
        {
            var stopWatch = Stopwatch.StartNew();
            long sumTotal = 0;

            var threads = new List<Thread>();
            var partitionSize = array.Length / Environment.ProcessorCount;
            
            for (var index = 0; index < array.Length; index+=partitionSize)
            {
                var thread = new Thread(
                    state =>
                    {
                        long sum = 0;
                        if (state is IEnumerable<int> partition)
                            foreach (var i in partition)
                                sum += i;
                        else
                            throw new ArgumentNullException(nameof(partition));

                        Interlocked.Add(ref sumTotal, sum);
                    });
                
                thread.Start(array.Skip(index).Take(partitionSize));
                threads.Add(thread);
            }

            foreach (var thread in threads)
                thread.Join();

            stopWatch.Stop();
            return new Result("thread", array.Length, stopWatch.Elapsed, sumTotal);
        }

        static Result LinqOperation(int[] array)
        {
            var stopWatch = Stopwatch.StartNew();
            var sumTotal = array.AsParallel().Aggregate((long)0, (item1, item2) => item1 + item2);
            stopWatch.Stop();
            return new Result("linq", array.Length, stopWatch.Elapsed, sumTotal);
        }
        
        static Result ParallelOperation(int[] array)
        {
            var stopWatch = Stopwatch.StartNew();
            
            long sumTotal = 0;
            Parallel.ForEach(
                Partitioner.Create(0, array.Length), 
                new ParallelOptions {MaxDegreeOfParallelism = Environment.ProcessorCount},
                range =>
                {
                    long sum = 0;
                    for (int i = range.Item1; i < range.Item2; i++)
                        sum += array[i];

                    Interlocked.Add(ref sumTotal, sum);
                });
            
            stopWatch.Stop();
            return new Result("parallel", array.Length, stopWatch.Elapsed, sumTotal);
        }
        
        static Result SimpleOperation(int[] array)
        {
            var stopWatch = Stopwatch.StartNew();
            
            decimal sum = 0;
            foreach (var item in array)
                sum += item;

            stopWatch.Stop();
            return new Result("simple", array.Length, stopWatch.Elapsed, sum);
        }
        
    }
}