using System;

namespace dotnetcore;

public class Result
{
    private readonly int _range;
    private readonly TimeSpan _elapsed;
    private readonly string _name;
    private readonly decimal _sum;

    public Result(string name, int range, TimeSpan elapsed, decimal sum)
    {
        _elapsed = elapsed;
        _name = name;
        _sum = sum;
        _range = range;
    }

    public override string ToString()
        => $"Operation name: {_name} range: {_range} elapsed time: {_elapsed:c} sum: {_sum}";
}